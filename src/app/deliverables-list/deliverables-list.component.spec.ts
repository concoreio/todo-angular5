import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverablesListComponent } from './deliverables-list.component';

describe('DeliverablesListComponent', () => {
  let component: DeliverablesListComponent;
  let fixture: ComponentFixture<DeliverablesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverablesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverablesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
