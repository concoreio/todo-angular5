import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import Deliverable from '../deliverable/deliverable'
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import * as moment from 'moment';

declare const Concore: any;

class SearchMetadata {
  count: number;
  execution: number;
  limit: number;
  offset: number;
  page: number;
}

enum STATE {
  inbox = "inbox",
  blocked = "blocked",
  done = "done"
}

enum LOADSTATE {
  loaded = "loaded",
  loading = "loading"
}

@Component({
  selector: 'app-deliverables-list',
  templateUrl: './deliverables-list.component.html',
  styleUrls: ['./deliverables-list.component.css'],
  animations: [
  trigger('listState', [
    state('loaded', style({
      maxHeight: '400px',
      minHeight: '0px'
    })),
    state('loading', style({
      maxHeight: '0px',
      minHeight: '0px'
    })),
    transition('loaded => loading', animate('400ms ease-out')),
    transition('loading => loaded', animate('800ms ease-in'))
  ])
]
})
export class DeliverablesListComponent implements OnInit {
  deliverables: Array<any>;
  meta: SearchMetadata;
  showDelayed: Boolean;
  totalDue: Number;
  state: STATE;
  loadState: LOADSTATE;
  moleculoid: any;

  constructor() { }

  ngOnInit() {
    this.deliverables = [];
    this.getMoleculoid();
    this.getDeliverables();
  }

  getMoleculoid() {
    Concore.Datacore.Moleculoid.get('entregaveis')
      .then(mol => {
        this.moleculoid = mol;
      });
  }

  getDeliverables() {
    this.loadState = LOADSTATE.loading;
    this.state = STATE.inbox;
    const { MoleculeQuery, Auth } = Concore.Datacore;

    const query = new MoleculeQuery('entregaveis');
    query.equalTo('responsavel.id', Auth.getUser().getId());
    query.notEqualTo('status.value', 'publicado');
    query.notEqualTo('status.value', 'arquivado');
    query.addDescending('milestone');
    query.find()
      .then(resp => {
        this.meta = resp.metadata.resultset;
        this.deliverables = resp.results;

        if (this.meta.count > 0) {
          this.countDueDeliverables();
          setTimeout(() => {
            this.loadState = LOADSTATE.loaded;
          }, 500);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  getPending() {
    this.loadState = LOADSTATE.loading;
    this.state = STATE.blocked;
    const { MoleculeQuery, Auth } = Concore.Datacore;

    const query = new MoleculeQuery('entregaveis');
    query.equalTo('responsavel.id', Auth.getUser().getId());
    query.equalTo('status.value', 'pendencia');
    query.addDescending('milestone');
    query.find()
      .then(resp => {
        this.meta = resp.metadata.resultset;
        this.deliverables = resp.results;

        if (this.meta.count > 0) {
          this.countDueDeliverables();
          setTimeout(() => {
            this.loadState = LOADSTATE.loaded;
          }, 500);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  getDone() {
    this.loadState = LOADSTATE.loading;
    this.state = STATE.done;
    const { MoleculeQuery, Auth } = Concore.Datacore;

    const query = new MoleculeQuery('entregaveis');
    query.equalTo('responsavel.id', Auth.getUser().getId());
    query.equalTo('status.value', 'publicado');
    query.addDescending('milestone');
    query.find()
      .then(resp => {
        this.meta = resp.metadata.resultset;
        this.deliverables = resp.results;

        if (this.meta.count > 0) {
          this.countDueDeliverables();
          setTimeout(() => {
            this.loadState = LOADSTATE.loaded;
          }, 500);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  onMolDelete(molID: any) {
    this.deliverables = this.deliverables.filter(item => item.getId() !== molID);
    this.countDueDeliverables();
  }

  onMolUpdate(mol: any) {
    this.deliverables = this.deliverables.filter(item => item.getId() !==  mol.getId());
    this.deliverables.unshift(mol);
    this.countDueDeliverables();
  }

  countDueDeliverables() {
    const count = this.deliverables.filter(item => {
      const status = item.getAtoms('status').value;
      if (status === 'publicado' || status === 'pendencia') {
        return false;
      }

      if (this.isAfter(item.getAtoms('milestone'))) {
        return true;
      }

      return false;
    })

    this.totalDue = count.length;
  }

  isAfter(date: Date) {
    return moment().isAfter(date);
  }
}
