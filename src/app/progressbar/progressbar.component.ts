import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

class State {
  label: string;
  selected: boolean;

  constructor(label: string, selected: boolean) {
    this.label = label;
    this.selected = selected;
  }
}

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.css']
})
export class ProgressbarComponent implements OnInit {
  @Input() states: Array<string>;
  @Input() selected: string;

  _states: Array<State>;
  constructor() { }

  ngOnInit() {
    console.log(`states: ${this.states}`);
    console.log(`selected: ${this.selected}`);
    this.init();
  }

  ngOnChanges() {
    this.init();
  }

  isCurrent(state) {
    if (this.selected === state) {
      return true;
    }

    false;
  }

  init() {
    if (!this.states) {
      return;
    }

    if (!this.selected) {
      return;
    }

    this._states = [];
    let isSelected = true;

    this.states.forEach(item => {
      const nState = new State(item, isSelected);
      this._states.push(nState);

      if (item === this.selected) {
        isSelected = false;
      }
    });
  }
}
