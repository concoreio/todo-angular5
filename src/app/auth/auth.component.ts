import { Component, OnInit } from '@angular/core';
declare var Concore: any;

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  username: string;
  password: string;
  isLogged: boolean;
  auth: any;
  user: any;


  constructor() { }

  ngOnInit() {
    this.auth = Concore.Datacore.Auth;
    this.isLogged = this.auth.isLogged();

    if (this.isLogged) {
      this.user = this.auth.getUser().toJSON();
    }
  }

  login() {
    const { Auth } = Concore.Datacore;
    Auth.login(this.username, this.password)
      .then(usuario => {
        this.isLogged = this.auth.isLogged();
        this.user = usuario.toJSON();
        console.log(usuario.toJSON());
      })
      .catch(error => {
        console.error(error);
      });
  }

  logout() {
    const { Auth } = Concore.Datacore;
    Auth.logout()
      .then(usuario => {
        this.isLogged = this.auth.isLogged();
        this.user = null;
      })
      .catch(error => {
        console.error(error);
      });
  }

}
