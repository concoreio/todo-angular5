class Tuple {
  value: string;
  label: string;
}

class Sprint {
  titulo: string;
  id: string;
}

export default class Deliverable {
  id: number;
  title: string;
  status: Tuple;
  tipo: string;
  storyPoints: number;
  estimativa: number;
  projeto: string;
  sprint: Sprint;
  responsavel: string;
  processo: string;
  descricao: string;
  inicio: Date;
  previsao: Date;
  entrega: Date;
  links: Array<string>;
  anexos: string;

  constructor(id, title, descricao) {
    this.id = id;
    this.title = title;
    this.descricao = descricao;
  }
}