import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import Deliverable from '../deliverable/deliverable';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import * as moment from 'moment';
moment.locale('pt-br');

declare var Concore: any;

@Component({
  selector: 'app-deliverable',
  templateUrl: './deliverable.component.html',
  styleUrls: ['./deliverable.component.css'],
  animations: [
    trigger('hoverState', [
      state('false', style({
        height: 0,
        opacity: 0,
        overflow: 'hidden'
      })),
      state('true',   style({
        opacity: 1,
        overflow: 'hidden'
      })),
      transition('false => true', animate('200ms ease-in')),
      transition('true => false', animate('200ms ease-out'))
    ])
  ]
})

export class DeliverableComponent implements OnInit {
  @Input() deliverable: any;
  @Input() moleculoid: any;
  @Output() onDelete = new EventEmitter<any>();
  @Output() onUpdate = new EventEmitter<any>();
  hover: boolean;
  errorMessage: string;
  states: Array<string>;

  constructor() { }

  ngOnInit() {
    this.hover = false;
    this.getStates();
  }

  ngOnChanges() {
    this.getStates();
  }

  getStates() {
    if (!this.moleculoid) {
      return;
    }

    this.states = this.moleculoid.getMetatoms('status').getOptions().map(item => item.label);
  }

  changeStatus(deliverable: any, status: string) {
    deliverable.set('status', status);
    deliverable.save()
      .then(mol => {
        deliverable = mol;
      })
      .catch(error => {
        console.error(error);
      });
  }

  delete(deliverable: any) {
    deliverable.destroy()
      .then(mol => {
        this.onDelete.emit(deliverable.getId());
      })
      .catch(error => {
        console.error(error);
      });
  }

  toggle() {
    this.hover = !this.hover;
  }

  addToSprint(deliverable: any) {
    const query = new Concore.Datacore.MoleculeQuery('sprint');
    query.lessThanOrEqualTo('inicio', new Date());
    query.greaterThanOrEqualTo('entrega', new Date());

    query.first()
      .then(sprint => {
        if (!sprint) {
          throw new Error("Não foi encontrado o sprint atual");
        }

        deliverable.set('sprint', sprint.getId());
        deliverable.set('status', 'sprintbacklog');
        deliverable.set('inicio', sprint.getAtoms('inicio'));

        if (deliverable.getAtoms('estimativa')) {
          deliverable.set('milestone', moment(sprint.getAtoms('inicio')).add(deliverable.getAtoms('estimativa').hours, 'hour').toDate());
        } else {
          deliverable.set('milestone', sprint.getAtoms('entrega'));
        }

        return deliverable.save();
      })
      .then(mol => {
        deliverable = mol;
        this.onUpdate.emit(deliverable);
      })
      .catch(err => {
        this.errorMessage = err.message;
      });
  }

  backToBacklog(deliverable: any) {
    deliverable.removeAtoms('sprint');
    deliverable.removeAtoms('inicio');
    deliverable.removeAtoms('milestone');
    deliverable.set('status', 'backlog');
    deliverable.save()
      .then(mol => {
        deliverable = mol;
        this.onUpdate.emit(deliverable);
      })
      .catch(error => {
        console.error(error);
      });
  }

  isAfter(date: Date) {
    return moment().isAfter(date);
  }

  isDue() {
    const status = this.deliverable.getAtoms('status').value;

    if (status === 'publicado' || status === 'pendencia') {
      return false;
    }

    if (!this.deliverable.getAtoms('milestone')) {
      return false;
    }

    const dDate = this.deliverable.getAtoms('milestone');
    return this.isAfter(dDate);
  }

  isBefore(date: Date) {
    return moment().isBefore(date);
  }

  mouseEnter() {
    this.hover = true;
  }

  mouseLeave() {
    this.hover = false;
  }

  shouldWarn() {
    if (this.deliverable.getAtoms('status').value === 'publicado') {
      return false;
    }

    const date = this.deliverable.getAtoms('milestone');
    const time = this.deliverable.getAtoms('estimativa');
    if (!date) {
      return false;
    }

    if (!time) {
     return false
    }

    if (moment().isAfter(date)) {
      return false;
    }

    return moment().add(time.hours, 'hour').isAfter(date);
  }

}
