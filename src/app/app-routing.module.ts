import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeliverablesListComponent } from './deliverables-list/deliverables-list.component';

const routes: Routes = [
  { path: '', component: DeliverablesListComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {

}
